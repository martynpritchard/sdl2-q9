//For exit()
#include <stdlib.h>
//For printf()
#include <stdio.h>
//For round()
#include <math.h>
#if defined(_WIN32) || defined(_WIN64)
//The SDL library
#include "SDL.h"
//Support for loading different types of images.
#include "SDL_image.h"
#else
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#endif


const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;

int main(int argc, char* args[])
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;
    // Temporary surface used while loading the image
    SDL_Surface* temp = nullptr;
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture* backgroundTexture = nullptr;

    // Player variables
// Player sprite
    SDL_Texture* playerTexture = nullptr;
    // Sprite size
    const int PLAYER_SPRITE_WIDTH = 64;
    const int PLAYER_SPRITE_HEIGHT = 64;
    // Source and target rectangle
    SDL_Rect standRight; //standing sprite
    SDL_Rect targetRectangle;
    float playerSpeed = 50.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;

        // Window control
        SDL_Event event;
        bool quit = false; //false
        //Input - keys/joysticks?
        float verticalInput = 0.0f;
        float horizontalInput = 0.0f;

        // Timing variables
        unsigned int currentTimeIndex;
        unsigned int prevTimeIndex;
        unsigned int timeDelta;
        float timeDeltaInSeconds;

    
    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    ////////////////////////////////////////
      

/**********************************
* Setup background sprite *
* ********************************/
// Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");
    if (temp == nullptr)
    {
        printf("Background image not found!");
    }
    // Create a texture object from the loaded image
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);
    // Clean-up - done with "image" now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;
    /**********************************
    * Setup mech sprite *
    * ********************************/
    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/walker1.png");
    if (temp == nullptr)
    {
        printf("Walker image not found!");
    }
    // Create a texture object from the loaded image
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);
    // Clean-up - done with "temp" now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    targetRectangle.x = 0; //can't use these to track position
    targetRectangle.y = 0; //due to small position changes
    targetRectangle.w = PLAYER_SPRITE_WIDTH;
    targetRectangle.h = PLAYER_SPRITE_HEIGHT;

    standRight.x = 2 * PLAYER_SPRITE_WIDTH;
    standRight.y = 2 * PLAYER_SPRITE_HEIGHT;
    standRight.w = PLAYER_SPRITE_WIDTH;
    standRight.h = PLAYER_SPRITE_HEIGHT;

    prevTimeIndex = SDL_GetTicks();
    // Game loop
    while (!quit) // while quit is not true
    {
        // Calculate time elapsed
        // Better approaches to this exist -
        // https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks(); //time in milliseconds
        timeDelta = currentTimeIndex - prevTimeIndex;
        timeDeltaInSeconds = timeDelta * 0.001;
        // Handle input
        if (SDL_PollEvent(&event)) // test for events
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
                // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                case SDLK_UP:
                    verticalInput = -1.0f;
                    break;
                case SDLK_DOWN:
                    verticalInput = 1.0f;
                    break;
                }
                break;

                // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    // Nothing to do here.
                    break;
                case SDLK_UP: // fall through.
                case SDLK_DOWN:
                    verticalInput = 0.0f;
                    break;
                }
                break;
            default:
                // not an error, there's lots we don't handle.
                break;
            }
        }

        // Process Player Input
// Calculate player velocity.
// Note: This is imperfect, no account taken of diagonal!
        float xVelocity = verticalInput * playerSpeed;
        float yVelocity = horizontalInput * playerSpeed;
        // Update Player
        // Calculate distance travelled since last update
        float yMovement = timeDeltaInSeconds * xVelocity;
        float xMovement = timeDeltaInSeconds * yVelocity;
        // Update player position.
        playerX += xMovement;
        playerY += yMovement;
        // Move sprite to nearest pixel location.
        targetRectangle.y = round(playerY);
        targetRectangle.x = round(playerX);

        // Render (draw stuff)
        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and
        // alpha (transparency) values (ie. RGBA)
        SDL_RenderClear(gameRenderer);
        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);
        SDL_RenderCopy(gameRenderer,
            playerTexture,
            &standRight,
            &targetRectangle);
        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);
    }// big while!

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;
    //Shutdown SDL - clear up resources etc.
    SDL_Quit();
    // Exit the program
    exit(0);
}